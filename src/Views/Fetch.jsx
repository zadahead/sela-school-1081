import { useEffect, useState } from "react"
import { Rows } from "UIKit"

export const Fetch = () => {
    const [list, setList] = useState(null);

    useEffect(() => {
        setTimeout(() => {
            fetch('https://jsonplaceholder.typicode.com/todos')
                .then(resp => resp.json())
                .then(json => {
                    setList(json);
                })
        }, 2000);
    }, [])

    console.log('render')
    const renderList = () => {
        return list.map(i => {
            return <h3 key={i.id}>{i.title}</h3>
        })
    }

    return (
        <Rows>
            {!list ? (
                <h1>loading...</h1>
            ): (
                renderList()
            )}
        </Rows>
    )
}