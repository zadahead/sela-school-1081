import { Rows, Fetch } from "UIKit";


export const FetchHookUser = () => {
    const render = (item) => {
        return <h3>{item.name}</h3>
    }

    //reder
    return (
        <Rows>
            <h2>Users list</h2>
            <Fetch url='/users/1' onRender={render} />
        </Rows>
    )
}

export const FetchHook = () => {
    const render = (list) => {
        return list.map(i => {
            return <h3 key={i.id}>{i.name}</h3>
        })
    }

    //reder
    return (
        <Rows>
            <h2>Users list</h2>
            <Fetch url='/users' onRender={render} />
        </Rows>
    )
}

export const FetchHookTodos = () => {

    const render = (list) => {
        return list.map(i => {
            return <h3 key={i.id}>{i.title}</h3>
        })
    }

    //reder
    return (
        <Rows>
            <h2>Todos list</h2>
            <Fetch url='/todos' onRender={render} />
        </Rows>
    )
}

